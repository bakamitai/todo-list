/*
 * TODO: LOL!!!!
 * Maybe allow the use of negative relative dates
 * Make the todo list printing better
 * Allow matching entries by date.
 * I still havent implemented the daily feature yet LOL
 * Come up with a way to test the functionality of the program so that I can test if my changes break anything
 * Find more efficient way to do add_days_to_date
 * Fix the date related code to handle leap years properly LOL!!! EPIC
 * Refactor add/done/remove section. There is a lot of repeated code
 * Implement undo feature. It would be very sad to accidentally delete the wrong entry and not be able to recover it.
 */
#define STRING_IMPLEMENTATION
#include "string.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <ctype.h>
#include <assert.h>

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define TODO_FILENAME "todo.txt"

#define panic(format, ...) _panic(__FILE__, __LINE__, (format) __VA_OPT__(,) __VA_ARGS__)
void _panic(const char *file, int line, const char *format, ...)
{
    fprintf(stderr, "Program panicked at: %s:%d\n", file, line);
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    exit(1);
}

typedef struct
{
    int year;
    int month;
    int day;
} Date;

typedef struct
{
    String message;
    Date date;
} Entry;

typedef struct
{
    Entry *data;
    size_t length;
    size_t capacity;
} EntryArray;

// ANSI escape codes to change the color of text printed to the terminal.
#define FOREGROUND_BLACK          "\033[30m"
#define FOREGROUND_RED            "\033[31m"
#define FOREGROUND_GREEN          "\033[32m"
#define FOREGROUND_YELLOW         "\033[33m"
#define FOREGROUND_BLUE           "\033[34m"
#define FOREGROUND_MAGENTA        "\033[35m"
#define FOREGROUND_CYAN           "\033[36m"
#define FOREGROUND_WHITE          "\033[37m"
#define FOREGROUND_BRIGHT_BLACK   "\033[90m"
#define FOREGROUND_BRIGHT_RED     "\033[91m"
#define FOREGROUND_BRIGHT_GREEN   "\033[92m"
#define FOREGROUND_BRIGHT_YELLOW  "\033[93m"
#define FOREGROUND_BRIGHT_BLUE    "\033[94m"
#define FOREGROUND_BRIGHT_MAGENTA "\033[95m"
#define FOREGROUND_BRIGHT_CYAN    "\033[96m"
#define FOREGROUND_BRIGHT_WHITE   "\033[97m"

#define BACKGROUND_BLACK          "\033[40m"
#define BACKGROUND_RED            "\033[41m"
#define BACKGROUND_GREEN          "\033[42m"
#define BACKGROUND_YELLOW         "\033[43m"
#define BACKGROUND_BLUE           "\033[44m"
#define BACKGROUND_MAGENTA        "\033[45m"
#define BACKGROUND_CYAN           "\033[46m"
#define BACKGROUND_WHITE          "\033[47m"
#define BACKGROUND_BRIGHT_BLACK   "\033[100m"
#define BACKGROUND_BRIGHT_RED     "\033[101m"
#define BACKGROUND_BRIGHT_GREEN   "\033[102m"
#define BACKGROUND_BRIGHT_YELLOW  "\033[103m"
#define BACKGROUND_BRIGHT_BLUE    "\033[104m"
#define BACKGROUND_BRIGHT_MAGENTA "\033[105m"
#define BACKGROUND_BRIGHT_CYAN    "\033[106m"
#define BACKGROUND_BRIGHT_WHITE   "\033[107m"

#define BOLD                      "\033[1m"
#define UNDERLINE                 "\033[4m"
#define RESET                     "\033[0m"

// Some other constants used when reading and writing data to a file.
#define END_MESSAGE 0
#define START_DATE  1
#define END_TODO    3

static int little_endian;

// I don't care about leap years right now. They aren't real
static int month_days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
static int month_days_cumulative[12] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};



int date_difference(Date d1, Date d2)
{
    return d1.year * 365 - d2.year * 365 +
        month_days_cumulative[d1.month - 1] - month_days_cumulative[d2.month - 1] +
        d1.day - d2.day;
}

void add_days_to_date(Date *d, int days)
{
    // This might change at some point but I think most of the time people will be adding
    // future dates to their todo list not past dates.
    assert(days >= 0);
    
    while(days--)
    {
        d->day++;

        if(d->day > month_days[d->month - 1])
        {
            d->day = 1;
            d->month++;

            if(d->month > 12)
            {
                d->month = 1;
                d->year++;
            }
        }
    }
}

Date unpack_date(int date)
{
    Date d;
    d.year = date / 10000;
    d.month = (date % 10000) / 100;
    d.day = date % 100;

    return d;
}

int pack_date(Date d)
{
    return d.year * 10000 + d.month * 100 + d.day;
}

int validate_date(Date d)
{
    return d.year > 1900 && d.year < 10000
        && d.month >= 1 && d.month <= 12
        && d.day >= 1 && d.day <= month_days[d.month - 1];
}

void push_entry(EntryArray *arr, Entry e)
{
    if(arr->length >= arr->capacity)
    {
        arr->data = realloc(arr->data, sizeof(Entry) * arr->length * 2);
        arr->capacity = arr->length * 2;
    }
    arr->data[arr->length++] = e;
}

int pop_entry(EntryArray *arr, Entry *output)
{
    if(arr->length > 0)
    {
        *output = arr->data[--arr->length];
        return 1;
    }
    return 0;
}

int remove_entry(EntryArray *arr, size_t index)
{
    // If the order being changed doesn't matter a more efficient way
    // to do this would be to just copy the last entry to the index entry.
    if(!arr || index >= arr->length) return 0;

    for(; index < arr->length - 1; index++)
    {
        arr->data[index] = arr->data[index + 1];
    }
    arr->length--;
    return 1;
}

void parse_file(String file_contents, EntryArray *todo_array, EntryArray *done_array)
{
    Entry entry;
    int date_number;
    u8 *bytes = (u8 *) file_contents.data;
    u32 cursor = 0;
    EntryArray *current_array = todo_array;

    while(cursor < file_contents.length)
    {
        if(bytes[cursor] == START_DATE)
        {
            if(cursor + 4 >= file_contents.length)
                panic("Expected a four byte integer representing a date.\n");
            cursor++;

            // This is to make sure that the bytes are read in the correct order
            // depending on the endianness of the computer. The bytes are always
            // written to disk in little endian order.

            u8 *date_bytes = (u8 *) &date_number;
            if(little_endian)
            {
                *date_bytes++ = bytes[cursor++];
                *date_bytes++ = bytes[cursor++];
                *date_bytes++ = bytes[cursor++];
                *date_bytes++ = bytes[cursor++];
            }
            else
            {
                *date_bytes++ = bytes[cursor + 3];
                *date_bytes++ = bytes[cursor + 2];
                *date_bytes++ = bytes[cursor + 1];
                *date_bytes++ = bytes[cursor];

                cursor += 4;
            }

            if(cursor >= file_contents.length)
                panic("Expected a message after the date.\n");

            Date d = unpack_date(date_number);

            if(!validate_date(d))
                panic("Invalid date.\nyear = %d\nmonth = %d\nday = %d\ndate number = %d\n", d.year, d.month, d.day, date_number);

            entry.date = d;
            entry.message.data = (char *) bytes + cursor; 
            entry.message.length = 0;

            for(;;)
            {
                if(isprint(bytes[cursor]))
                {
                    entry.message.length++;
                    cursor++;

                    if(cursor >= file_contents.length)
                        panic("Reached end of file before end of message.\n");
                }
                else if(bytes[cursor] == END_MESSAGE)
                {
                    cursor++;
                    break;
                }
                else
                    panic("Non printable character found in message.\n");
            }
            entry.message.capacity = entry.message.length;

            push_entry(current_array, entry);
        }
        else if(bytes[cursor] == END_TODO)
        {
            cursor++;
            current_array = done_array;
        }
        else
            panic("I dunno mate files fucked ay.\n");
    }
}

int int_input(char *message, int min, int max)
{
    char line[128];
    int result = 0, input = 0x80000000;

    do
    {
        if(message) puts(message);
        fgets(line, 127, stdin);
        result = sscanf(line, "%d", &input);
        message = "Invalid input";
    } while(result != 1 || input < min || input > max);

    return input;
}

int main(int argc, char **argv)
{
    // Variables to store information provided by command line arguments
    int add = 0, remove = 0, done = 0, print = 1;
    String match_string = {0}, add_message = {0};
    Date add_date, todays_date;

    // Get todays date and store it in todays_date
    time_t now = time(NULL);
    struct tm *today = localtime(&now);
    todays_date.year = today->tm_year + 1900;
    todays_date.month = today->tm_mon + 1;
    todays_date.day = today->tm_mday;

    add_date = todays_date;

    // Parse command line arguments
    // Now because of the --noprint option the for loop makes sense because I don't care if
    // --noprint is at the beginning or the end of the argument list.
    for(int i = 1; i < argc; i++)
    {
        if(strncmp(argv[i], "--noprint", strlen("--noprint")) == 0)
            print = 0;
        else if(strncmp(argv[i], "add", strlen("add")) == 0)
        {
            add = 1;
            if(i + 1 >= argc) panic("The add option requires a message after.\n");

            add_message = wrap_cstring(argv[++i]);
            
            if(i + 1 < argc && strncmp(argv[++i], "--date", strlen("--date")) == 0)
            {
                if(i + 1 >= argc) panic("The --date option was not followed by a date.\n");
                
                // This whole section could probably be better. I don't know how right now but I feel it in my bones
                char last_character = argv[i + 1][strlen(argv[i + 1]) - 1];
                if(last_character == 'w' || last_character == 'W' || last_character == 'd' || last_character == 'D' ||
                   last_character == 'y' || last_character == 'Y' || last_character == 'm' || last_character == 'M')
                {
                    long relative_days = strtol(argv[++i], NULL, 10); 
                    if(relative_days == 0) panic("Could not parse %s as a number.\n", argv[i]);

                    switch(last_character)
                    {
                        case 'd':
                        case 'D':
                        add_days_to_date(&add_date, relative_days);
                        break;

                        case 'w':
                        case 'W':
                        add_days_to_date(&add_date, relative_days * 7);
                        break;

                        case 'm':
                        case 'M':
                        add_days_to_date(&add_date, relative_days * 28);
                        break;

                        case 'y':
                        case 'Y':
                        add_days_to_date(&add_date, relative_days * 365); // Leap years still don't exist
                        break;
                    }
                }
                else
                {
                    int old_errno = errno;
                    long date_number = strtol(argv[++i], NULL, 10);

                    if(date_number == 0)   panic("Could not parse %s as a number.\n", argv[i]);
                    if(errno != old_errno) panic("Could not parse %s as a number.\n\t%s\n", argv[i], strerror(errno));

                    add_date = unpack_date((int) date_number);

                    if(!validate_date(add_date)) panic("%d is not a valid date.\n", date_number);
                }
            }
        }
        else if(strncmp(argv[i], "remove", strlen("remove")) == 0)
        {
            remove = 1;
            if(i + 1 >= argc) panic("The remove option requires a match string after.\n");

            match_string = wrap_cstring(argv[++i]);
        }
        else if(strncmp(argv[i], "done", strlen("done")) == 0)
        {
            done = 1;
            if(i + 1 >= argc) panic("The done option requires a match string after.\n");
            
            match_string = wrap_cstring(argv[++i]);
        }
        else
        {
            printf("What was that? %s\n", argv[i]);
        }
    }
    
    // todo_string and done_string need to be initialized to zero. OKAY?!
    String todo_file;
    char empty_list = END_TODO;
    EntryArray todo_array, done_array;

    // Test for endianness the first of the two bytes will be 1 if the computer is little endian
    // and 0 if the computer is big endian.
    u16 two_bytes = 1;
    u8 *first_byte = (u8 *) &two_bytes;
    little_endian = *first_byte;

    // Read file named TODO_FILENAME into the string todo_file
    if(!read_to_string(TODO_FILENAME, &todo_file))
    {
        // If TODO_FILENAME does not exist set todo_file to a string representing an empty todo list.
        if(errno == ENOENT)
        {
            todo_file.data = &empty_list;
            todo_file.length = 1;
            todo_file.capacity = 1;
        }
        else
            panic("Could not open file " TODO_FILENAME ".\n\t%s\n", strerror(errno));
    }
    todo_file = strip(todo_file); // get rid of dirty little whitespace
    
    // Initialize todo_array, allocate enough memory for 64 entries
    todo_array.data = (Entry *) malloc(sizeof(Entry) * 64);
    todo_array.length = 0;
    todo_array.capacity = 64;

    // Initialize done_array, allocate enough memory for 64 entries
    done_array.data = (Entry *) malloc(sizeof(Entry) * 64);
    done_array.length = 0;
    done_array.capacity = 64;

    // A whole bunch of stuff here has been replaced by this function.
    parse_file(todo_file, &todo_array, &done_array);
    
    if(add)
    {
        Entry e;
        e.message = add_message;
        e.date = add_date;
        push_entry(&todo_array, e);
    }
    else if(remove)
    {
        int *todo_indices, *done_indices, todo_indices_length = 0, done_indices_length = 0, indices_capacity = 64;
        todo_indices = (int *) malloc(sizeof(int) * indices_capacity);
        done_indices = (int *) malloc(sizeof(int) * indices_capacity);

        for(int i = 0; i < todo_array.length; i++)
        {
            if(fuzzy_match(match_string, todo_array.data[i].message))
            {
                todo_indices[todo_indices_length++] = i;
                if(todo_indices_length >= indices_capacity) panic("Not implemented yet vro.\n");
            }
        }

        for(int i = 0; i < done_array.length; i++)
        {
            if(fuzzy_match(match_string, done_array.data[i].message))
            {
                done_indices[done_indices_length++] = i;
                if(done_indices_length >= indices_capacity) panic("Not implemented yet vro.\n");
            }
        }

        if(todo_indices_length == 1 && done_indices_length == 0)
            remove_entry(&todo_array, todo_indices[0]);
        else if(todo_indices_length == 0 && done_indices_length == 1)
            remove_entry(&done_array, done_indices[0]);
        else if(todo_indices_length + done_indices_length > 1)
        {
            printf("Multiple entries match.\n");
            int count = 1, remove_index = 0;

            for(int i = 0; i < todo_indices_length; i++)
            {
                printf("%d. ", count++);
                print_string(todo_array.data[todo_indices[i]].message);
                putchar('\n');
            }
            for(int i = 0; i < done_indices_length; i++)
            {
                printf("%d. ", count++);
                print_string(done_array.data[done_indices[i]].message);
                putchar('\n');
            }

            remove_index = int_input(NULL, 1, todo_indices_length + done_indices_length);

            if(remove_index > todo_indices_length)
            {
                remove_index -= todo_indices_length + 1;
                remove_entry(&done_array, done_indices[remove_index]);
            }
            else
            {
                remove_index--;
                remove_entry(&todo_array, todo_indices[remove_index]);
            }
        }
    }
    else if(done)
    {
        int *todo_indices = malloc(sizeof(int) * 64);
        int todo_indices_length = 0;

        for(int i = 0; i < todo_array.length; i++)
        {
            if(fuzzy_match(match_string, todo_array.data[i].message))
            {
                todo_indices[todo_indices_length++] = i;
                if(todo_indices_length >= 64) panic("It doesn't resize yet DICKHEAD!\n");
            }
        }

        if(todo_indices_length == 1)
        {
            Entry e = todo_array.data[todo_indices[0]];
            remove_entry(&todo_array, todo_indices[0]);
            push_entry(&done_array, e);
        }
        else if(todo_indices_length > 1)
        {
            for(int i = 0; i < todo_indices_length; i++)
            {
                printf("%d. ", i + 1);
                print_string(todo_array.data[todo_indices[i]].message);
                putchar('\n');
            }

            int choice = int_input(NULL, 1, todo_indices_length);

            Entry e = todo_array.data[todo_indices[choice - 1]];
            remove_entry(&todo_array, todo_indices[choice - 1]);
            push_entry(&done_array, e);
        }
    }

    // Print the todo list entries to the terminal
    if(print)
    {
        printf("Todo:\n");
        for(int i = 0; i < todo_array.length; i++)
        {
            Entry e = todo_array.data[i];
            int day_difference = date_difference(e.date, todays_date);

            putchar('\t');
            if(day_difference > 0)
            {
                printf(FOREGROUND_BRIGHT_GREEN);
                print_string(e.message);
                printf("[+%d days]", day_difference);
            }
            else if(day_difference < 0)
            {
                printf(FOREGROUND_RED);
                print_string(e.message);
                printf("[%d days]", day_difference);
            }
            else
            {
                printf(FOREGROUND_BLUE);   
                print_string(e.message);
            }
            printf(RESET);
            putchar('\n');
        }

        printf("Done:\n");
        for(int i = 0; i < done_array.length; i++)
        {
            Entry e = done_array.data[i];
            int day_difference = date_difference(e.date, todays_date);

            putchar('\t');
            if(day_difference > 0)
            {
                printf(FOREGROUND_BRIGHT_GREEN);
                print_string(e.message);
                printf("[+%d days]", day_difference);
            }
            else if(day_difference < 0)
            {
                printf(FOREGROUND_RED);
                print_string(e.message);
                printf("[%d days]", day_difference);
            }
            else 
            {
                printf(FOREGROUND_BLUE);
                print_string(e.message);
            }
            printf(RESET);
            putchar('\n');
        }
    }

    // Only write changes to file if there are any changes
    if(add || done || remove)
    {
        FILE *tf = fopen(TODO_FILENAME, "w");
        if(tf)
        {
            int date_number;
            Entry e;
            for(int i = 0; i < todo_array.length; i++)
            {
                e = todo_array.data[i];
                date_number = pack_date(e.date);

                fputc(START_DATE, tf);

                // Always write bytes to disk in little endian order
                if(little_endian)
                    fwrite(&date_number, 1, 4, tf);
                else
                {
                    u8 *byte = (u8 *) &date_number;
                    fputc(byte[3], tf);
                    fputc(byte[2], tf);
                    fputc(byte[1], tf);
                    fputc(byte[0], tf);
                }
                fwrite(e.message.data, 1, e.message.length, tf);
                fputc(END_MESSAGE, tf);
            }
            // This is here so that if the todo list is empty after a remove operation the TODO_FILENAME
            // file represents an empty todo list. The file actually being empty breaks the program
            // LOL!!!!!
            fputc(END_TODO, tf);

            for(int i = 0; i < done_array.length; i++)
            {
                e = done_array.data[i];
                date_number = pack_date(e.date);

                fputc(START_DATE, tf);

                // Always write bytes to disk in little endian order
                if(little_endian)
                    fwrite(&date_number, 1, 4, tf);
                else
                {
                    u8 *byte = (u8 *) &date_number;
                    fputc(byte[3], tf);
                    fputc(byte[2], tf);
                    fputc(byte[1], tf);
                    fputc(byte[0], tf);
                }
                fwrite(e.message.data, 1, e.message.length, tf);
                fputc(END_MESSAGE, tf);
            }

            fclose(tf);
        }
        else
        {
            panic("Could not open file " TODO_FILENAME ".\n\t%s\n", strerror(errno));
        }
    }
    return 0;
}
