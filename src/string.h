#ifndef STRING
#define STRING
#include <stddef.h>

typedef struct
{
    char *data;
    size_t length;
    size_t capacity;
} String;

typedef struct
{
    String base;
    char delimiter;
    int cursor;
} StringIterator;

int next(StringIterator *iter, String *output);
int number_of_tokens(StringIterator iter);
int read_to_string(char *filename, String *output);
String wrap_cstring(char *str);
String copy_string(String s);
int parse_int(String s);
String strip(String s);
int find(String s, char c);
String split_left(String s, size_t index);
String split_right(String s, size_t index);
String substring(String s, size_t start, size_t end);
int common_chars(String a, String b);
int equal(String a, String b);
int fuzzy_match(String a, String b);
void print_string(String s);

#endif

#ifdef STRING_IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>

int next(StringIterator *iter, String *output)
{
    if(iter->cursor >= iter->base.length) return 0;

    output->data = iter->base.data + iter->cursor;
    output->length = 0;

    while(iter->cursor < iter->base.length && iter->base.data[iter->cursor++] != iter->delimiter) output->length++;
    
    output->capacity = output->length;
    return 1;
}

int number_of_tokens(StringIterator iter)
{
    int n;
    for(n = 0; iter.cursor < iter.base.length; ++n)
    {
        while(iter.cursor < iter.base.length && iter.base.data[iter.cursor++] != iter.delimiter);
    }
    return n;
}

int read_to_string(char *filename, String *output)
{
    struct stat statbuf = {0};

    if(stat(filename, &statbuf) == 0)
    {
        FILE *f;
        if((f = fopen(filename, "r")))
        {
            output->data = (char *) malloc(statbuf.st_size);
            output->length = output->capacity = statbuf.st_size;
            fread(output->data, 1, statbuf.st_size, f);
            fclose(f);
        }
        else
        {
            /* fprintf(stderr, "Unable to open file %s\n\t%s\n", filename, strerror(errno)); */
            return 0;
        }
    }
    else
    {
        /* fprintf(stderr, "Unable to open file %s\n\t%s\n", filename, strerror(errno)); */
        return 0;
    }

    return 1;
}

String wrap_cstring(char *str)
{
    String s;
    s.data = str;
    s.length = strlen(str);
    s.capacity = s.length;
    return s;
}

String copy_string(String s)
{
    String new;
    new.data = (char *) malloc(s.length);
    new.length = s.length;
    new.capacity = s.length;
    memcpy(new.data, s.data, s.length);
    return new;
}

int parse_int(String s)
{
    int order = 1;
    int result = 0;
    for(int i = s.length - 1; i >= 0; i--)
    {
        result += (s.data[i] - '0') * order;
        order *= 10;
    }
    return result;
}

String strip(String s)
{
    char c;
    
    while((c = s.data[s.length - 1]) == ' ' || c == '\t' || c == '\n' || c == '\r') s.length--;
    while((c = s.data[0]) == ' ' || c == '\t' || c == '\n' || c == '\r')
    {
        s.data++;
        s.length--;
    }

    return s;
}

int find(String s, char c)
{
    for(int i = 0; i < s.length; i++)
    {
        if(s.data[i] == c) return i;
    }

    return -1;
}

String split_left(String s, size_t index)
{
    assert(index < s.length);

    String left;
    left.data = s.data;
    left.length = index + 1;
    left.capacity = left.length;

    return left;
}

String split_right(String s, size_t index)
{
    assert(index < s.length);
    
    String right;
    right.data = s.data + index;
    right.length = s.length - index;
    right.capacity = right.length;

    return right;
}

String substring(String s, size_t start, size_t end)
{
    String sub;
    sub.data = s.data + start;
    sub.length = end - start;
    sub.capacity = sub.length;
    return sub;
}

int common_chars(String a, String b)
{
    int common = 0;
    for(int i = 0; i < a.length; i++)
    {
        for(int j = 0; j < b.length; j++)
        {
            if(a.data[i] == b.data[j])
            {
                common++;
                break;
            }
        }
    }
    return common;
}

int equal(String a, String b)
{
    if(a.length != b.length) return 0;

    for(int i = 0; i < a.length; i++)
        if(a.data[i] != b.data[i]) return 0;
    return 1;
}

char lowercase(char c)
{
    return c + (c >= 'A' && c <= 'Z') * 32;
}

int fuzzy_match(String a, String b)
{
    int b_pos = 0, match;
    char c1, c2;
    
    for(int i = 0; i < a.length; ++i)
    {
        match = 0;

        c1 = lowercase(a.data[i]);
        while(b_pos < b.length)
        {
            c2 = lowercase(b.data[b_pos++]);
            if(c1 == c2)
            {
                match = 1;
                break;
            }
        }
    }
    return match;
}

void print_string(String s)
{
    fwrite((void *) s.data, sizeof(char), s.length, stdout);
}

#endif
